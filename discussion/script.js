function printStar(){
    console.log("*");
}

printStar();

function sayHello(name){
    console.log("Hello " + name)
};

sayHello("Joshua");

// function alertPrint(){
//     alert("Hello")
//     console.log("Hello")
// };

// alertPrint();

function add(x,y){
    let sum = x + y
    console.log(sum)
}

add(1,2);
add(10,44);
add(8,20);

function printBio(fname, lname, age){
    // console.log("Hello " + fname + " " + lname + " " + age)

    console.log(`Hello ${fname} ${lname} ${age}`)
};

printBio("Marco","Kalalo",12);

function createFullName(fname, mname, lname){
    return `${fname} ${mname} ${lname}`
};

let fullName = createFullName("Juan","Dela","Cruz");
console.log(fullName);